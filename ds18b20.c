
#include <avr/io.h>
#include <util/delay.h>
#include "ds18b20.h"

uint8_t DS18B20_init() {

	unsigned char oneWireState;

	ONEWIRE_HIGH;
	ONEWIRE_DIR_OUT;
	ONEWIRE_LOW;
	_delay_us(600);

	ONEWIRE_DIR_IN;
	_delay_us(70);

	oneWireState = ONEWIRE_STATE;
	_delay_us(200);

	ONEWIRE_HIGH;
	ONEWIRE_DIR_OUT;
	_delay_us(600);

	return oneWireState == 0;
}

void DS18B20_write(uint8_t data) {

	for (uint8_t i = 0; i < 8; i++) {
		ONEWIRE_HIGH;
		ONEWIRE_DIR_OUT;

		ONEWIRE_LOW; (0x01 & data) ? _delay_us(7) : _delay_us(70); // 8 - 80
		ONEWIRE_HIGH; (0x01 & data) ? _delay_us(70) : _delay_us(7);

		data >>= 1;
	}
}

uint8_t DS18B20_read() {

	uint8_t data = 0;
	for (uint8_t i = 1; i != 0; i <<= 1 ) {
		ONEWIRE_HIGH;
		ONEWIRE_DIR_OUT;
		ONEWIRE_LOW;
		_delay_us(2);
		ONEWIRE_DIR_IN;
		_delay_us(5);
		data |= (ONEWIRE_STATE != 0) * i;
		_delay_us(60);
	}
	return data;
}

uint16_t DS18B20_getTemp(int16_t *temperature) {

	uint8_t sp[9];

	if (DS18B20_init() == 0) return 0;

	DS18B20_write(DS18B20_COMMAND_SKIP_ROM);
	DS18B20_write(DS18B20_COMMAND_READ_SP);

	for(uint8_t i = 0; i < 9; i++)
		sp[i] = DS18B20_read();

	if ((sp[0]|sp[1]|sp[2]|sp[3]|sp[4]|sp[5]|sp[6]|sp[7]) == 0) return 0;

//	*temperature = (float)(sp[0] + (sp[1] << 8)) / 16;
	*temperature = (int16_t)((sp[1] << 8 ) + sp[0]) >> 4;
	return 1;
}

uint8_t DS18B20_convert() {

	if (DS18B20_init() == 0) return 0;

	DS18B20_write(DS18B20_COMMAND_SKIP_ROM);
	DS18B20_write(DS18B20_COMMAND_CONVERT);

	return 1;
}
