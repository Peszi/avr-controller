/*
 * ds18b20.h
 *
 * Created: 04.01.2020 14:30:17
 *  Author: Peszi
 */ 

#ifndef DS18B20_H_
	#define DS18B20_H_
#endif /* DS18B20_H_ */

#define ONEWIRE_PIN					PD3
#define ONEWIRE_DIR					DDRD
#define ONEWIRE_PINS				PIND
#define ONEWIRE_PORT				PORTD

#define ONEWIRE_DIR_IN				ONEWIRE_DIR &= ~(1 << ONEWIRE_PIN)
#define ONEWIRE_DIR_OUT				ONEWIRE_DIR |= (1 << ONEWIRE_PIN)

#define ONEWIRE_LOW					ONEWIRE_PORT &= ~(1 << ONEWIRE_PIN)
#define ONEWIRE_HIGH				ONEWIRE_PORT |= (1 << ONEWIRE_PIN)

#define ONEWIRE_STATE				(ONEWIRE_PINS & (1 << ONEWIRE_PIN))

#define DS18B20_COMMAND_READ_ROM   	0x33 //!< Read sensors's ROM address
#define DS18B20_COMMAND_MATCH_ROM  	0x55 //!< Request sensors to start matching ROM addresses
#define DS18B20_COMMAND_SKIP_ROM   	0xCC //!< Request sensors to ignore ROM matching phase
#define DS18B20_COMMAND_CONVERT    	0x44 //!< Request temperature conversion
#define DS18B20_COMMAND_WRITE_SP   	0x4E //!< Request internal scratchpad to be written
#define DS18B20_COMMAND_READ_SP    	0xBE //!< Read data from the internal scratchpad
#define DS18B20_COMMAND_COPY_SP    	0x48 //!< Request scratchpad contents to be copied into internal EEPROM
#define DS18B20_COMMAND_SEARCH_ROM 	0xF0 //!< Begin ROM discovery proccess

uint8_t DS18B20_init();

void DS18B20_write(uint8_t data);
uint8_t DS18B20_read();

uint16_t DS18B20_getTemp(int16_t *temperature);
uint8_t DS18B20_convert();
