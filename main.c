/*
 * avr-furnance.c
 *
 * Created: 28.12.2019 19:54:16
 * Author : Peszi
 */ 

#include <avr/io.h>
#include <util/delay.h>
#include <stdio.h>
#include <stdlib.h>
#include "include/hd44780.h"
#include "util.h"
#include "display.h"
#include "ds18b20.h"
#include "setting.h"
#include "usart.h"

typedef enum {
	TEMP = 0, MENU = 1, SETTING = 2
} ControllerView; 

ControllerView currentView = TEMP;

int16_t rawTemperature = 0;
uint8_t currentTemperature;

uint8_t secondTimer = 0;
uint8_t temperatureTimer = 0;

uint8_t fanEnable = 0;
uint8_t timeWarnAlarm = 0;

void onModeChanged(void) {
	switch(currentView) {
		case TEMP: 
		
		break;
		case MENU:
		
		break;
		case SETTING: 
		
		break;
	}
}

void onEverySecondUpdate(void) {
	SETTING_update_timers();
}

void onDisplayTemperature(void) {
	lcd_clrscr();

	LCD_write_big_digits(0, currentTemperature);
	lcd_goto(0x04); lcd_putc('o');
	
	if (alarmMute) {
		lcd_goto(0x0f); lcd_putc('M');
	}
	
	if (fanEnable) {
		lcd_goto(0x4f); lcd_putc('*');
	}
	
	//lcd_goto(0x49); lcd_puts("12:45");
	//lcd_goto(0x4f); lcd_putc('Y');

	//LCD_write_digit(0x06, alarmMuteTime);
	//LCD_write_digit(0x09, warningAlarm.trigger);
	//LCD_write_digit(0x46, alarmMute);
	//LCD_write_digit(0x49, dangerAlarm.setting);
}

void onTemperatureUpdate(void) {
	// read temp
	DS18B20_getTemp(&rawTemperature);
	if (!DS18B20_convert()) { rawTemperature = 0; }
	// setup temp value
	currentTemperature = (uint8_t)(rawTemperature);
	if (currentTemperature > 99) { currentTemperature = 99; }
		
	// update alarms
	uint8_t trigger = warningAlarm.value <= currentTemperature;
	if (trigger && !warningAlarm.trigger) {
		alarmShortTimer = 0;
	}
	warningAlarm.trigger = trigger;
	
	trigger = dangerAlarm.value <= currentTemperature;
	if (trigger && !dangerAlarm.trigger) {
		alarmMute = 0;
	}
	dangerAlarm.trigger = trigger;
	
	alarmAnyTrigger = warningAlarm.trigger || dangerAlarm.trigger;
	
	// display temp value
	if (currentView == TEMP) {
		onDisplayTemperature();
	}
}

void onUpdate(void) {
	
	UTIL_set_fan(fanEnable);
		
	if (secondTimer >= 15) {
		secondTimer = 0;
		onTemperatureUpdate();
	}

	if (temperatureTimer >= 20) {
		temperatureTimer = 0;
		onEverySecondUpdate();
	}
	
	switch(currentView) {
		case TEMP: break;
		case MENU: SETTING_update_menu(); break;
		case SETTING: SETTING_update_change(); break;
	}
}

int main(void) {
	
	lcd_init();
	
	UTIL_init_IO();
	SETTING_alarm_load_data();
	DS18B20_init();
	
	while(1) {
		
		UTIL_update_IO();
		onUpdate();
		
		if (!alarmMute && alarmAnyTrigger && btnChange) {
			btnChange = 0; // block btn state (just mute);
			alarmMute = 0x01;
			alarmMuteTime = alarmMuteValue; // some value
		}
		
		if (UTIL_is_btn_pressed(BTN_OK) && UTIL_inc_dec_val(&currentView, 3, 1)) {
			onModeChanged();
		}
		if (UTIL_is_btn_pressed(BTN_ESC)) {
			if (UTIL_inc_dec_val(&currentView, 0, 0)) {
				onModeChanged();
			} else {
				fanEnable ^= 0x01;
			}
		}
		
		UTIL_end_IO();
		secondTimer++;
		temperatureTimer++;
		
		_delay_ms(50);
	}
}