/*
 * setting.h
 *
 * Created: 04.01.2020 14:43:04
 *  Author: Peszi
 */ 


#ifndef SETTING_H_
	#define SETTING_H_
	extern uint8_t alarmMute;
	extern uint16_t alarmMuteTime;
	extern uint8_t alarmAnyTrigger;
	extern uint16_t alarmMuteValue;
	extern uint8_t alarmShortTimer;
#endif /* SETTING_H_ */

#define SETTING_DEFAULT_ALARM_TEMP_VALUE	50		// oC
#define SETTING_DEFAULT_ALARM_MUTE_VALUE	5		// mins
#define SETTING_ALARM_MAX_MUTE				120		// mins

#define SETTING_WARN_ALARM_SEC_ON			15		// secs
#define SETTING_WARN_ALARM_SEC_OFF			120		// secs

struct AlarmSetting {
	uint8_t value;
	uint8_t setting;
	uint8_t trigger;
} warningAlarm, dangerAlarm;

void SETTING_alarm_load_data(void);

void SETTING_update_menu(void);

void SETTING_update_change(void);

struct AlarmSetting SETTING_get_warn_alarm(void);

struct AlarmSetting SETTING_get_danger_alarm(void);

void SETTING_update_timers(void);